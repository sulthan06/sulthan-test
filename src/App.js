import React from 'react';
import './App.css';
import { createBrowserHistory } from "history";
import HomePage from './pages/HomePages';
import DetailPage from './pages/HomePages/DetailPage'
import {
  Router,
  Route,
  Switch
} from "react-router-dom";
const history = createBrowserHistory();

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Router history={history} basename={process.env.PUBLIC_URL}>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route
              exact
              path={`${process.env.PUBLIC_URL}/detail/:id`}
              component={DetailPage}
            />
          </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App;
