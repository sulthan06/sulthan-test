
function DetailPage() {
    const data = ['092289', '992299', '12291', '982289', '22022022', '2301', '2013', '1001', '756564', '1011', '766567', '756546', '2002', '91019', '765567'];
    const sorting = [];

    const palindromCheck = (str) => {
        var value = str === str.split('').reverse().join('');
        return value ? str : null;
    }
    data.forEach(row => {
        const valid = palindromCheck(row);
        if (valid) sorting.push(row)
    })
    return <div>
       {JSON.stringify(sorting)}
    </div>
}
export default DetailPage