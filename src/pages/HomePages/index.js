import React from 'react';
import { useSelector } from "react-redux";
import PhotoList from '../../features/PhotoList';
import PhotoSearchForm from "../../features/SearchPhoto"
import { searchPhotosLoaded } from '../../features/SearchPhoto/actions';

function HomePage(props) {
    const {history} = props;
    const state = useSelector(searchPhotosLoaded);
    return <div>
        <PhotoSearchForm />
        <PhotoList state={state} history={history}/>
    </div>
}
export default HomePage