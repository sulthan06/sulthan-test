import watchDetailPhoto from "./features/PhotoList/sagas"
import watchSearchPhotos from "./features/SearchPhoto/sagas"

export default function* rootSaga() {
    yield watchSearchPhotos()
    yield watchDetailPhoto()
}
