import { configureStore } from '@reduxjs/toolkit';
import photoReducer from './features/SearchPhoto/reducer';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: {
    photoReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware, logger),
  devTools: process.env.NODE_ENV !== 'production'
});
sagaMiddleware.run(rootSaga);

export { store };