
import styles from './modal.module.css';

function ModalPhoto({ data, show, onClose }) {
    return <>
        <div className={styles.modalWindow}>
            <div className={styles.modalClose} onClick={onClose}>Close</div>
            <img src={data.urls.small} alt={data.alt_description} width={200} height={200} />
        </div>
    </>
}

export default ModalPhoto