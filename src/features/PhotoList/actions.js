import { DETAIL_PHOTO, DETAIL_PHOTO_SUCCESS, DETAIL_PHOTO_ERROR } from './types';

export const detailPhoto = (payload) => ({
    type: DETAIL_PHOTO,
    payload
});

export function detailPhotoSuccess(keyword, photos, page) {
    return {
        type: DETAIL_PHOTO_SUCCESS,
        keyword,
        photos,
        page
    };
}
export const detailPhotoLoaded = (state) => state.photoReducer;
export function detailPhotoError(error) {
    return {
        type: DETAIL_PHOTO_ERROR,
        error,
    };
}