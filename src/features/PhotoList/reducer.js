import { DETAIL_PHOTO, DETAIL_PHOTO_SUCCESS, DETAIL_PHOTO_LOADED, DETAIL_PHOTO_ERROR } from "./types";

const initialState = {
    detail: null
};

export default function photoReducer(state = initialState, action) {
    switch (action.type) {
        case DETAIL_PHOTO:
            debugger
            return {
                ...state
            };
        case DETAIL_PHOTO_SUCCESS:
            debugger
            return {
                ...state,
                detail: action.photos,
                error: null,
                loading: false,
            };
        case DETAIL_PHOTO_LOADED:
            return {
                ...state
            };
        case DETAIL_PHOTO_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        default:
            return state;
    }
}
