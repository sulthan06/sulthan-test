import { call, put, takeEvery } from 'redux-saga/effects';
import { detailPhotoSuccess, detailPhotoError } from './actions';
import axios from 'axios';

const API_KEY = 'ghX9dF3ZzX6RU-GeqPLM21U5sQgVcrR13Rk-OAzq2G4';

function* getDetailPhoto(action) {
    try {
        const response = yield call(
            axios.get,
            `/photos/${action?.payload?.id ?? ''}&client_id=${API_KEY}`
        );
        const photos = response.data.results;
        yield put(detailPhotoSuccess(photos));
    } catch (error) {
        yield put(detailPhotoError(error));
    }
}

function* watchDetailPhoto() {
    yield takeEvery('DETAIL_PHOTO', getDetailPhoto);
}

export default watchDetailPhoto;