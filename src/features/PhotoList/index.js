
import React, { useEffect, useRef, useState } from 'react';
import styles from './photolist.module.css';
import { searchPhotos } from '../SearchPhoto/actions';
import { useDispatch } from 'react-redux';

function PhotoList(props) {
    const { state, history } = props;

    function handleClickDetail(id) {
        history.push(`${process.env.PUBLIC_URL}/detail/${id}`);
    }
    //console.log(state);
    const dispatch = useDispatch();
    const handleInfiniteScroll = () => {
        // statenya ke reset ,sudah di console datanya ada, need help 
        dispatch(searchPhotos({ keyword: state.keyword, page: state.page + 1 }));
    }

    const observer = useRef(new IntersectionObserver(
        (entries) => {
            const firstEntry = entries[0];
            if (firstEntry.isIntersecting) {
                handleInfiniteScroll()
            }
        },
        { threshold: 1 }
    ));

    const [element, setElement] = useState(null);

    useEffect(() => {

        //Wrap in if statement to avoid observing null.
        if (element) {
            observer.current.observe(element);
        }
        return () => {
            if (element) {
                // eslint-disable-next-line react-hooks/exhaustive-deps
                observer.current.unobserve(element);
            }
        };
    }, [element]);

    return <>
        {state.loading && <h3>Loading. . .</h3>}
        {state.photos.length > 0 &&
            <div className={styles.container}>
                {state.photos.map((photo) => (
                    <div key={photo.id}>
                        <div className={styles.item} onClick={() => handleClickDetail(photo.id)}>
                            <img src={photo.urls.small} alt={photo.alt_description} width={200} height={200} />
                        </div>
                    </div>
                ))}
            </div>
        }
        {state.isMore && <div ref={setElement}></div>}
        {state.photos.length > 0 && !state.isMore && <h2 className="fancy-h2 text-center">The End!</h2>}
    </>
}

export default PhotoList