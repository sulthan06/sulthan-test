import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { searchPhotos } from './actions';

function PhotoSearchForm() {
    const [keyword, setKeyword] = useState('');
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(searchPhotos({ keyword: keyword, page: 1 }));
    }

    return (
        <>
            <form onSubmit={handleSubmit}>
                <input type="text" value={keyword} onChange={(e) => setKeyword(e.target.value)} />
                <button type="submit">Cari</button>
            </form>
        </>
    );
}

export default PhotoSearchForm;
