import { SEARCH_PHOTOS, SEARCH_PHOTOS_SUCCESS, SEARCH_PHOTOS_LOADED, SEARCH_PHOTOS_ERROR } from './types';

export const searchPhotos = (query) => ({
    type: SEARCH_PHOTOS,
    payload: query
});

export function searchPhotosSuccess(keyword, photos, page) {
    return {
        type: SEARCH_PHOTOS_SUCCESS,
        keyword,
        photos,
        page
    };
}
export function searchPhotosLoaded(state) {
    return {
        type: SEARCH_PHOTOS_LOADED,
        ...state.photoReducer
    };
}
export function searchPhotosError(error) {
    return {
        type: SEARCH_PHOTOS_ERROR,
        error,
    };
}