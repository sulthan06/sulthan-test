import { SEARCH_PHOTOS, SEARCH_PHOTOS_SUCCESS, SEARCH_PHOTOS_LOADED, SEARCH_PHOTOS_ERROR } from "./types";

const initialState = {
    photos: [],
    error: null,
    loading: false,
    isMore: false,
    keyword: '',
    page: 1
};

export default function photoReducer(state = initialState, action) {
    switch (action.type) {
        case SEARCH_PHOTOS:
            return {
                ...state,
                keyword: action.payload.keyword,
                page: action.payload.page,
                loading: true
            };
        case SEARCH_PHOTOS_SUCCESS:
            return {
                ...state,
                photos: [...state.photos, ...action.photos],
                error: null,
                loading: false,
                isMore: action.photos.length > 0
            };
        case SEARCH_PHOTOS_LOADED:
            return {
                ...state
            };
        case SEARCH_PHOTOS_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                isMore: false
            };
        default:
            return state;
    }
}
