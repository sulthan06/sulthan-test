import { call, put, takeEvery } from 'redux-saga/effects';
import { searchPhotosSuccess, searchPhotosError } from './actions';
import axios from 'axios';

const API_KEY = 'ghX9dF3ZzX6RU-GeqPLM21U5sQgVcrR13Rk-OAzq2G4';

function* searchPhotos(action) {
    try {
        const searchTerm = `query=${action?.payload?.keyword ?? 'undefined'}`;
        const page = action?.payload?.page ?? 1;
        const response = yield call(
            axios.get,
            `https://api.unsplash.com/search/photos?${searchTerm}&page=${page}&client_id=${API_KEY}`
        );
        const photos = response.data.results;
        yield put(searchPhotosSuccess(action?.payload?.keyword ?? '', photos, page + 1));
    } catch (error) {
        yield put(searchPhotosError(error));
    }
}

function* watchSearchPhotos() {
    yield takeEvery('SEARCH_PHOTOS', searchPhotos);
}

export default watchSearchPhotos;